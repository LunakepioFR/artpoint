import data from "../../data.json";
import Image from "next/image";

function Artist({ params }: { params: { slug: string } }) {
  const artist = data.artists.find((artist) => artist.slug === params.slug);
  return artist ? (
    <main>
      <div className="artist">
        <Image
          src={artist.image}
          alt={`Portrait de ${artist.name}`}
          width={400}
          height={400}
        />
        <h1>{artist.name}</h1>
        <p>
          {artist.texts.map((artistText, index) => {
            return <span key={index}>{artistText.body}</span>;
          })}
        </p>
      </div>

      <div className="artworks">
        {artist.artworks.map((artwork, index) => (
          <div key={index} className="artwork">
            <p>
              {artwork.texts.map((artwork, index) => {
                return artwork.body;
              })}
            </p>
          </div>
        ))}
      </div>
    </main>
  ) : null;
}

export default Artist;
