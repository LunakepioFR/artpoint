import Image from "next/image";
import data from "./data.json";
import { useLayoutEffect } from "react";
import Link from "next/link";

export default function Home() {
  console.log(data);

  return (
    <main>
      <div className="title">
        <h1>{data.title.toUpperCase()}</h1>
        <p>{data.text}</p>
      </div>
      <div className="artists">
        {data.artists.map((artist, index) => (
          <Link href={`/artist/${artist.slug}`} key={index}>
            <div key={index} className="artist">
              <h2>{artist.name.replace(" ", "\n")}</h2>
              <Image
                src={artist.image}
                alt={`Portrait de ${artist.name}`}
                width={90}
                height={90}
              />
            </div>
          </Link>
        ))}
      </div>
    </main>
  );
}
